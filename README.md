# 11ty tailwind boilerplate

## Getting started

Open the project folder in terminal and enter:

```
npm install
```

After the install finishes, enter:

```
npm run start
```

You now have a local server running on http://localhost:8080  
All the code changes will now automatically update without refreshing the page (except CSS).

Open a secondary tab in terminal. Open the project folder and enter:

```
npm run tailwind
```

Tailwind watcher now runs in the background and automatically compiles the CSS as well.

## Building for production

Open the project folder in terminal and enter:

```
npm run build
```

After it finishes, enter:

```
npm run tailwind-build
```

And you should be ready to go.
