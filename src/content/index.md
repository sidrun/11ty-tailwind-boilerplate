---json
{
  "layout": "index.liquid",
  "url": "/",
  "title": "Landing page",
  "description": "Example description for the page, primarly used for SEO",
  "custom-variable": "Hello world!"
}
---

## {{ title }}

Default content for the {{title}}. Written in [Markdown](https://www.markdownguide.org/basic-syntax/) and [Liquid](https://shopify.github.io/liquid/basics/introduction/).  
Also: {{ custom-variable }}
