module.exports = function (eleventyConfig) {
  // Copy necessary folders to dist folder
  eleventyConfig.addPassthroughCopy({ "src/assets": "assets" });
  eleventyConfig.addPassthroughCopy({ "src/images": "images" });
  eleventyConfig.addPassthroughCopy({ "src/javascripts": "javascripts" });
  // When using Tailwind CSS, this addPassthroughCopy is not needed
  // eleventyConfig.addPassthroughCopy({ "src/stylesheets": "stylesheets" });

  // Setup global variables
  eleventyConfig.addGlobalData("assets_path", "/assets");
  eleventyConfig.addGlobalData("images_path", "/images");
  eleventyConfig.addGlobalData("javascripts_path", "/javascripts");
  eleventyConfig.addGlobalData("stylesheets_path", "/stylesheets");

  return {
    // Setup folder structure
    dir: {
      input: "src/content",
      output: "dist",
      includes: "../components",
      layouts: "../layouts",
      data: "../data",
    },
  };
};
