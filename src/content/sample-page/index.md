---json
{
  "layout": "index.liquid",
  "url": "/sample-page",
  "title": "Sample page",
  "description": "Example description for the page, primarly used for SEO"
}
---

## {{ title }}

Default content for the {{title}}. Written in [Markdown](https://www.markdownguide.org/basic-syntax/) and [Liquid](https://shopify.github.io/liquid/basics/introduction/).